/**
 * adultcheck.min.js
 * Single js file to check the age bevore give access to shop/site
 * Have a nice day.
 *
 *
 * @version 1.0
 * @author  Thorsten Laing ( laing@web.de )
 * @updated 18.01.2018
 *
 */

var adultcheck = function () {

    // Base target div
    var MODUL = 'div#adultcheck';

    // Redirect url when access denied
    var REURL = 'https://www.rauchfrei-info.de/';

    return {

        // =========================================================================
        // CONSTRUCTOR
        // =========================================================================
        init: function () {

            // Check if Adult-Cookie ist set
            if(adultcheck.readAdultCookie() !== 'verified'){

                adultcheck.addTemplate();

            }
        },

        // =========================================================================
        // HANDLE BUTTONS
        // =========================================================================
        handleBtn: function () {

            console.debug('APP: HandleButtons');


            $(MODUL).find('button').on('click', function () {

                var action = $(this).data('action');

                // NO: Redirect to url
                if(action === 'no'){
                    window.location = "https://www.rauchfrei-info.de";
                }

                // YES: Hide Button | Show Birthselect
                if(action === 'yes'){

                    $(MODUL).find('div#buttonholder').hide();

                    $(MODUL).find('div#birthselect').show();
                }

                // BIRTHDAY: Check selcted birthday
                if(action === 'birthday'){

                    adultcheck.checkBirthday();
                }

            })
        },

        // =========================================================================
        // HANDLE VIEW
        // =========================================================================
        addTemplate: function () {

            // Add Template
            $('body').append('<div id="adultcheck"> <div class="form"> <h3>ALTERSPRÜFUNG</h3> <p> <b>Achtung:</b> um diesen Onlineshop zu nutzen, müssen Sie <b>mindestens 18 Jahre alt</b> sein.<br><br> </p> <div id="buttonholder"> <button data-action="yes">JA</button> <button data-action="no">NEIN</button> </div> <div id="birthselect"> <h3>Bitte geben Sie Ihr Geburtsdatum ein.</h3> <select name="day"><option value=""></option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option></select> &nbsp; <select name="month"><option value=""></option><option value="0">Jan.</option><option value="1">Febr.</option><option value="2">Mrz.</option><option value="3">Apr.</option><option value="4">Mai</option><option value="5">Jun.</option><option value="6">Jul.</option><option value="7">Aug.</option><option value="8">Sept.</option><option value="9">Okt.</option><option value="10">Nov.</option><option value="11">Dez.</option></select> &nbsp; <select name="year"><option value=""></option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option><option value="1989">1989</option><option value="1988">1988</option><option value="1987">1987</option><option value="1986">1986</option><option value="1985">1985</option><option value="1984">1984</option><option value="1983">1983</option><option value="1982">1982</option><option value="1981">1981</option><option value="1980">1980</option><option value="1979">1979</option><option value="1978">1978</option><option value="1977">1977</option><option value="1976">1976</option><option value="1975">1975</option><option value="1974">1974</option><option value="1973">1973</option><option value="1972">1972</option><option value="1971">1971</option><option value="1970">1970</option><option value="1969">1969</option><option value="1968">1968</option><option value="1967">1967</option><option value="1966">1966</option><option value="1965">1965</option><option value="1964">1964</option><option value="1963">1963</option><option value="1962">1962</option><option value="1961">1961</option><option value="1960">1960</option><option value="1959">1959</option><option value="1958">1958</option><option value="1957">1957</option><option value="1956">1956</option><option value="1955">1955</option><option value="1954">1954</option><option value="1953">1953</option><option value="1952">1952</option><option value="1951">1951</option><option value="1950">1950</option><option value="1949">1949</option><option value="1948">1948</option><option value="1947">1947</option><option value="1946">1946</option><option value="1945">1945</option><option value="1944">1944</option><option value="1943">1943</option><option value="1942">1942</option><option value="1941">1941</option><option value="1940">1940</option><option value="1939">1939</option><option value="1938">1938</option><option value="1937">1937</option><option value="1936">1936</option><option value="1935">1935</option><option value="1934">1934</option><option value="1933">1933</option><option value="1932">1932</option><option value="1931">1931</option><option value="1930">1930</option></select> <br><br> <button data-action="birthday">ABSENDEN</button> </div> </div> </div>');

            // Add CSS
            $(MODUL)
                .css('position', 'absolute')
                .css('top', 0)
                .css('left', 0)
                .css('width', '100%')
                .css('height', '100%')
                .css('backgroundColor', '#000')
                .css('opacity',  '0.95')
                .css('z-index', '999');

            $(MODUL).find('div.form')
                .css('width', '500px')
                .css('height', '300px')
                .css('padding', '15px')
                .css('border', '1px solid #000')
                .css('margin', '0 auto')
                .css('top', '25%')
                .css('position', 'relative')
                .css('text-align', ' center')
                .css('fontFamily', ' Arial')
                .css('fontSize', ' 18px')
                .css('backgroundColor', '#fff')
                .css('color', ' #222')
                .css('borderRadius', '5px');

            $(MODUL).find('h3')
                .css('fontSize', ' 26px')
                .css('color', ' #222')
                .css('fontFamily', ' Arial');

            $(MODUL).find('div.form > p')
                .css('fontSize', ' 18px')
                .css('color', ' #222')
                .css('fontFamily', ' Arial');


            $(MODUL).find('button')
                .css('border', '1px solid #222')
                .css('padding', '15px')
                .css('width', '140px')
                .css('font-size', '20px')
                .css('font-weight', 'bolder')
                .css('cursor', 'pointer')
                .css('borderRadius', '5px');

            $(MODUL).find('div#birthselect > button')
                .css('border', ' 1px solid #222')
                .css('padding', '5px')
                .css('width', '140px')
                .css('font-size', '20px')
                .css('font-weight', 'bolder')
                .css('cursor', 'pointer');

            $(MODUL).find('div#birthselect > h3')
                .css('fontSize', ' 20px')
                .css('margin', ' 10px')
                .css('fontFamily', ' Arial');


            $(MODUL).find('select')
                .css('font-size', '20px');



            // Hide Birth select on startup
            $(MODUL).find('div#birthselect')
                .css('display' ,'none');


            $(MODUL).find('button')
            .mouseenter(function() {
                $(this).css('color', '#fff')
                       .css('backgroundColor', '#222');
            })
            .mouseleave(function () {
                 $(this).css('color', '')
                        .css('backgroundColor', '');
            });

            
            


            adultcheck.handleBtn();


        },

        // =========================================================================
        // CHECK BIRTHDAY
        // =========================================================================
        checkBirthday: function () {

            console.debug('APP: Check Birthday');

            var day   = parseInt( $(MODUL).find('select[name="day"]').val() );
            var month = parseInt( $(MODUL).find('select[name="month"]').val());
            var year  = parseInt( $(MODUL).find('select[name="year"]').val());

            // Check day value
            if(isNaN(day)){   alert('Es wurde kein Tag angegeben!'); return false }

            // Check month value
            if(isNaN(month)){ alert('Es wurde kein Monat angegeben!'); return false }

            // Check month value
            if(isNaN(year)){  alert('Es wurde kein Jahr angegeben!'); return false }


            var date_input   = new Date(year, month, day);
            var date_current = new Date();

            var year_different = (date_current.getFullYear() - date_input.getFullYear());

            // Check Years
            if( year_different >= 18){

                // When its 18, we need to check the month
                if(year_different === 18){

                    if(date_input.getMonth() < date_current.getMonth()){

                        adultcheck.deniedAccess();
                        return false;
                    }

                    if(date_input.getMonth() === date_current.getMonth()){

                        if(date_input.getDate() > date_current.getDate()){

                            adultcheck.deniedAccess();
                            return false;
                        }
                    }
                }

                // POINT OF TRUST
                adultcheck.giveAccess();
            }
            else
            {
                adultcheck.deniedAccess();
                return false;
            }

        },

        // =========================================================================
        // HANDLE ACCESS
        // =========================================================================
        giveAccess: function () {

            $(MODUL).slideUp().remove();

            adultcheck.setAdultCookie();

        },

        deniedAccess: function () {

            window.location = REURL;
        },

        // =========================================================================
        // HANDLE COOKIES
        // =========================================================================
        setAdultCookie: function () {

            document.cookie = "_adultcheck=verified";
        },
        
        readAdultCookie: function () {

            var cname = '_adultcheck';

            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    }

}();

$(document).ready(function () {
    adultcheck.init()
});